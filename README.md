# Personalization Views

##INTRODUCTION
Adds integration of [Personalization](https://www.drupal.org/project/personalization) with [Views](https://www.drupal.org/project/views) module.

##REQUIREMENTS
This module requires the following module(s):
* Personalization (https://www.drupal.org/project/personalization)
* Views (https://www.drupal.org/project/views)

##INSTALLATION
* Install as you would normally install a contributed Drupal module. See: https://drupal.org/documentation/install/modules-themes/modules-7 for further information.
* Configure Personalization module. See more information on the modules page and in README.txt
* Create view with reference to taxonomy terms used for scoring content. It is possible to use data loaded from fields score, pages, searches, location and changed for viewing, filtering or sorting data

##Recommended modules
* Views Aggregator Plus (https://www.drupal.org/project/views_aggregator)
* Ajax Blocks (https://www.drupal.org/project/ajaxblocks)

##MAINTAINERS
Current maintainers:
* Oleksandr Lunov ([alunyov](https://www.drupal.org/user/103985))

Supporting organizations: 
* [EPAM Systems](https://www.drupal.org/epam-systems)
